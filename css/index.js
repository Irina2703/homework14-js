"use strict"

// Теоретичні питання
// 1. В чому полягає відмінність localStorage і sessionStorage ?
//  основна відмінність між localStorage і sessionStorage полягає у терміні збереження даних: localStorage зберігає дані назавжди, поки їх не видалити вручну, тоді як sessionStorage зберігає дані тільки під час життєвого циклу конкретної вкладки браузера.

//     2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage ?
// обробка чутливої інформації у localStorage чи sessionStorage потребує ретельного розгляду аспектів безпеки та використання відповідних методів для запобігання несанкціонованого доступу до цих даних.

//         3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера ?

// Коли завершується сеанс браузера, дані, збережені в sessionStorage, будуть автоматично видалені.SessionStorage призначений для тимчасового зберігання даних впродовж життєвого циклу поточної вкладки браузера.Як тільки сеанс браузера закривається, весь збережений у sessionStorage об'єкт буде очищений.


//             Практичне завдання:
// Реалізувати можливість зміни колірної теми користувача.
// Технічні вимоги:
// - Взяти готове домашнє завдання HW - 4 "Price cards" з блоку Basic HMTL / CSS.
// - Додати на макеті кнопку "Змінити тему".
// - При натисканні на кнопку - змінювати колірну гаму сайту(кольори кнопок, фону тощо) на ваш розсуд.При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// - Вибрана тема повинна зберігатися після перезавантаження сторінки.
//     Примітки:
// - при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
// - зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.



// Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.

 const changeBtn = document.querySelector("#change-btn");
 console.log(changeBtn);
changeBtn.addEventListener("click", () => {
    if (document.body.classList.contains('dark-theme')) {
       
        document.body.classList.remove('dark-theme');
        localStorage.setItem('currentTheme', 'light-theme');
    } else {
     
        document.body.classList.add('dark-theme');
        localStorage.setItem('currentTheme', 'dark-theme');
    }
});


const savedTheme = localStorage.getItem('currentTheme');
if (savedTheme) {
    document.body.classList.add(savedTheme);
} 









